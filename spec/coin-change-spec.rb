require 'rspec'
require_relative '../lib/coin-change.rb'

describe "CoinChange" do

  # create a new instance of the class before each test
  before :each do
    @changer = CoinChange.new
  end

  it "returns an empty array when input is zero" do
    expect(@changer.make_change(0)).to eq([])
  end

  it "returns a penny when input is 1 cent" do
    expect(@changer.make_change(1)).to eq(['penny'])
  end

  it "returns 2 pennies when input is 2 cents" do
    expect(@changer.make_change(2)).to eq(['penny', 'penny'])
  end

  it "returns a nickel when input is 5 cents" do
    expect(@changer.make_change(5)).to eq(['nickel'])
  end

  it "returns a nickel and a penny when input is 6 cents" do
    expect(@changer.make_change 6).to eq ['nickel', 'penny']
  end

  it "returns a dime when input is 10 cents" do
    expect(@changer.make_change(10)).to eq(['dime'])
  end

  it "returns a dime and a nickel when input is 15 cents" do
    expect(@changer.make_change(15)).to eq(['dime', 'nickel'])
  end

  it "returns a dime and a nickel and a penny when input is 16 cents" do
    expect(@changer.make_change(16)).to eq(['dime', 'nickel', 'penny'])
  end

  it "returns a quarter when input is 25 cents" do
    expect(@changer.make_change(25)).to eq(['quarter'])
  end

  it "returns a quarter and a dime when input is 35 cents" do
    expect(@changer.make_change(35)).to eq(['quarter', 'dime'])
  end

  it "returns a quarter, a dime and a nickel when input is 40 cents" do
    expect(@changer.make_change(40)).to eq(['quarter', 'dime', 'nickel'])
  end

  it "returns a quarter, a dime and a nickel and a penny when input is 41 cents" do
    expect(@changer.make_change(41)).to eq(['quarter', 'dime', 'nickel', 'penny'])
  end

end
