class CoinChange

  def initialize
    @coin = {
      25 => 'quarter',
      10 => 'dime',
      5 => 'nickel',
      1 => 'penny'
    }
  end

  def make_change(amount)
    result = []

    @coin.each_key do |coin_value|
      while amount >= coin_value
        result << @coin[coin_value]
        amount -= coin_value
      end
    end

    result
  end
end
